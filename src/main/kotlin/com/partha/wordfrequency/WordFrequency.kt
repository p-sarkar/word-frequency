package com.partha.wordfrequency

class WordFrequency {
    fun getFrequency(phrase: String, word: String): Int? {
        val words = phrase.toLowerCase().split("\\s".toRegex())
        val wordCounts = words
                .groupingBy { it }
                .eachCount()
        val wordCount = wordCounts[word.toLowerCase()]

        return wordCount ?: 0
    }

    fun getFrequencyMap(phrase: String): Map<String, Int> {
        return phrase
                .toLowerCase()
                .split("\\s".toRegex())
                .groupingBy { it }
                .eachCount()
                .toList()
                .sortedWith(compareBy({-it.second}, { it.first}))
                .toMap()
    }
}

fun main() {
    val phrase = "Although Java does not allow java one to express null-safety in its type system, Spring Framework, Spring Data, and " +
            "Reactor now provide null-safety of their API java via kotlin tooling-friendly annotations. By default, types from Java APIs " +
            "used in Kotlin are recognized kotlin as platform types for which kotlin null-checks are relaxed. Kotlin’s support for JSR " +
            "305 annotations kotlin combined with kotlin nullability annotations provide null-safety for the related Spring API in Kotlin"
    println(WordFrequency().getFrequencyMap(phrase))
}
