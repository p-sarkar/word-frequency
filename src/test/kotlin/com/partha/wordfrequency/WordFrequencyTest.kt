package com.partha.wordfrequency

import kotlin.test.Test
import kotlin.test.assertEquals

class WordFrequencyTest {
    @Test fun shouldReturnFrequencyOfFooAs5CaseInsensitive() {
        val classUnderTest = WordFrequency()
        val phrase = "Foo something foo other foo no foo why where foo"
        assertEquals(5, classUnderTest.getFrequency(phrase,"Foo"))
        assertEquals(5, classUnderTest.getFrequency(phrase,"foo"))
    }

    @Test fun shouldReturnFrequencyOfBarAs3FooAs2NoAs1ZingAs0() {
        val classUnderTest = WordFrequency()
        val phrase = "Bar something bar other bar no foo why where foo"
        assertEquals(3, classUnderTest.getFrequency(phrase,"Bar"))
        assertEquals(2, classUnderTest.getFrequency(phrase,"Foo"))
        assertEquals(0, classUnderTest.getFrequency(phrase,"zing"))
    }

    @Test fun shouldReturnFrequencyOfBarAs3FooAs2NoAs1ZingAs0InOrder() {
        val classUnderTest = WordFrequency()
        val phrase = "Bar something bar other a bar no foo why where foo"
        println("Sorted Map: ${classUnderTest.getFrequencyMap(phrase).toList()}")

        assertEquals(Pair("bar", 3), classUnderTest.getFrequencyMap(phrase).toList()[0])
        assertEquals(Pair("foo", 2), classUnderTest.getFrequencyMap(phrase).toList()[1])
        assertEquals(Pair("no", 1), classUnderTest.getFrequencyMap(phrase).toList()[3])
        assertEquals(Pair("why", 1), classUnderTest.getFrequencyMap(phrase).toList()[7])
    }
}
